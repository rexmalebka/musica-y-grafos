import networkx as nx
import matplotlib.pyplot as plt
from pythonosc import osc_message_builder
from pythonosc import udp_client
from time import sleep

host = '127.0.0.1'
port = 57120

client = udp_client.SimpleUDPClient(host, port)

#añadir aristas y vertices
G = nx.Graph()
G.add_nodes_from(range(69,77))
G.add_edges_from([(69,70,{'weight':1}),(69,72,{'weight':2}),(69,73,{'weight':2}),(69,74,{'weight':3}),(69,76,{'weight':3})])
G.add_edges_from([(70,75,{'weight':1}),(70,71,{'weight':1})])
G.add_edges_from([(71,76,{'weight':3}),(71,72,{'weight':1})])
G.add_edges_from([(72,73,{'weight':3})])
G.add_edges_from([(73,74,{'weight':2})])
G.add_edges_from([(74,75,{'weight':2})])
G.add_edges_from([(75,76,{'weight':1})])



#dibujar cosas 
pos = nx.spring_layout(G)
nx.draw_networkx_nodes(G, pos, node_size=700)
nx.draw_networkx_edges(G, pos, with_labels=True)
nx.draw_networkx_labels(G, pos, font_size=10, font_family='sans-serif')
nx.draw_networkx_edge_labels(G, pos, edge_labels=nx.get_edge_attributes(G,'weight'))

#definir una melodia
#melodia=[69,74,75,70,71,76,75,74,73,69,72,71,70,69,76,71,72,69,73,74,69,76,71,70,69,73,72,71,76,75,70,69,73,72,69,70,71,72,69,74,75,76,71,72,69,76,75,70,69,74,75,76,69]
melodias=set()
for k in range(70,77):
    for melodia in nx.all_simple_paths(G,69,k):
        for n,k in enumerate(melodia[1:]):
            print(n, melodia[n],G[melodia[n]][k]['weight'])
            client.send_message('/playgrafo',['nota',melodia[n]])
            sleep(G[melodia[n]][k]['weight']/10)
            client.send_message('/stopgrafo',[])





plt.axis('off')
plt.show()
