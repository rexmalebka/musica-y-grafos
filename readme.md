# Música con grafos

Prueba para realizar música con grafos.
Cada nodo del grafo es una nota musical en midi, cada conexión o arista es la duración en unidades de tiempo entre dos nodos.
Las conexiones fueron realizadas intencionalmente, igual la elección de las notas.

![ilustración de grafo](grafo.png "grafo de ejemplo" )

El sonido lo genera `Supercollider` a través de Comunicación OSC, la secuencia la realicé trazando todos los caminos posibles entre la nota 69 (A4) hasta la nota 76, resultando en 109 melodías.

Utilizo la librería `networkx` para trabajar con grafos, `pythonosc` para comunicarme con `supercollider` y `matplotlib` para plotear el resultado

Al final el [resultado](https://www.youtube.com/watch?v=9X2MO7FKQiI)
